import axios from 'axios';
const instance = axios.create({
  timeout: 10000,
});
instance.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);
instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    let errorStatus = error.response;
    if (error.code === 'ECONNABORTED') {
      alert('请求超时');
    }
    if (errorStatus === '404') {
      alert('请求未找到');
    } else if (errorStatus === '500') {
      alert('服务器错误');
    }
    return Promise.reject(error);
  }
);
export default instance;
