import axios from 'axios';
import httpCode from './httpCode';
import ErrorBoundarieUp from './logs';
// 开发环境  测试环境  预发布环境   生成环境
// 1. 超时请求 2.区分环境  3. 路径拼接 4.错误处理
const httpTool = ({
  timeout = 10,
  commonHeaders = {},
  failMessage = (msg) => {
    alert(msg);
  },
}) => {
  const httpAxios = axios.create({
    timeout: timeout * 100,
    baseURL: process.env.REACT_APP_BASE_URL,
  });

  // 添加请求拦截器
  httpAxios.interceptors.request.use(
    function (config) {
      // 在发送请求之前做些什么
      return {
        ...config,
        headers: {
          ...config.headers,
          ...commonHeaders,
        },
      };
    },
    function (error) {
      // 对请求错误做些什么
      return Promise.reject(error);
    }
  );

  // 添加响应拦截器
  httpAxios.interceptors.response.use(
    function (response) {
      console.log(response, 'response');
      // 对响应数据做点什么;
      if (response.data.statusCode == 200) {
        return response.data;
      }

      failMessage(
        response.data.msg ||
          httpCode[response.data.statusCode] ||
          '服务器出现未知错误，请重新请求'
      );
    },
    function (error) {
      // 对响应错误做点什么
      console.log(error, 'error**********');
      if (error.code == 'ECONNABORTED') {
        failMessage('请求超时');
        //收集错误信息 错误信息上报 埋点
        ErrorBoundarieUp({
          type: 'axios',
          error: {
            url: error.config.url,
            method: error.config.method,
          },
        });
      }
      return Promise.reject(error);
    }
  );
  return httpAxios;
};

export default httpTool;
