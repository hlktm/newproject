// 入口文件
import axios from '../utils/axios';
// import axios from '../utils/httpTool';
const context = require.context('./modules', false, /\.js$/);
// console.log(context.keys());
const dataObj = context.keys().reduce((prev, cur) => {
  // console.dir(context(cur));
  return { ...prev, ...context(cur).default };
}, {});

// console.log(dataObj);

const api = Object.keys(dataObj).reduce((prev, cur) => {
  // console.log(cur);
  prev[cur] = () => {
    return axios({
      ...dataObj[cur],
    });
  };
  return prev;
}, {});
console.log(api, 'api');

export default api;
