// 归档
const Pigeonhole = {
  getPigeonhole: {
    method: 'get',
    url: '/api/article/archives',
  },
};

export default Pigeonhole;
