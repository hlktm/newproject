// 文章标签
const LabelTab = {
  getTab: {
    method: 'get',
    url: '/api/tag',
  },
};

export default LabelTab;
