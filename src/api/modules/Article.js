// 文章列表
const Article = {
  getList: {
    method: 'get',
    url: '/api/article',
  },
  //tab
  TabList: {
    method: 'get',
    url: '/api/category',
  },
};

export default Article;
