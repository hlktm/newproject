const knowledge = {
  getlist: {
    method: 'get',
    url: '/api/knowledge',
  },
};

export default knowledge;
