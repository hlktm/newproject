import React, { Component } from 'react';
import api from '../../api';
import './Classify.less';
import { withRouter } from 'react-router-dom';
export class Classify extends Component {
  state = {
    classifyData: [],
    // arr: [
    //   { title: '所有', path: '/home/article' },
    //   { title: '前端', path: '/home/be' },
    //   { title: '后端', path: '/home/fe' },
    //   { title: 'Linux', path: '/home/linux' },
    //   { title: 'LeetCode', path: '/home/leetcode' },
    //   { title: '要闻', path: '/home/news' },
    // ],
  };
  async componentDidMount() {
    let res = await api.getClassify();
    this.setState(
      {
        classifyData: res.data.data,
      },
      () => {
        console.log(this.state.classifyData, 'classifyData');
      }
    );
  }
  render() {
    let { classifyData, arr } = this.state;
    return (
      <div className="left_classify">
        <h2>文章分类</h2>
        <ul>
          {classifyData &&
            classifyData.map((item, index) => {
              return (
                <li
                  key={index}
                  onClick={() => {
                    // this.props.history.push({
                    //   pathname: '/home/' + item.id,
                    //   state: item,
                    // });
                  }}
                >
                  <span>{item.label}</span>
                  <span>共计{item.articleCount}篇文章</span>
                </li>
              );
            })}
        </ul>
      </div>
    );
  }
}

export default withRouter(Classify);
