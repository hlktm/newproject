import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as ArticleAction from '../../store/model/Article/articleAction';
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
import Tab from '../Tab/Tab';
class List extends Component {
  state = {
    arr: this.props.arr,
  };
  render() {
    let { arr } = this.props;
    console.log(arr, 'rrr');
    return (
      <div className="Reading">
        {arr &&
          arr.length &&
          arr.map((item, index) => {
            return (
              <div
                key={index}
                className="concent"
                onClick={() => {
                  console.log(item);
                  this.props.history.push({
                    pathname: '/home/detail/' + item.id,
                    state: item,
                  });
                }}
              >
                <h1>
                  {item.title} {this.props.timer ? '' : ''}
                  <span>{item.category && item.category.label}</span>{' '}
                </h1>
                <div className="summary">
                  <p>{item.summary}</p>
                  <img src={item.cover} alt="" />
                </div>
                <div className="ico">
                  <span>
                    {' '}
                    <HeartOutlined />
                    {item.likes}
                  </span>
                  <span>
                    {' '}
                    <EyeOutlined /> {item.views}
                  </span>
                  <span>
                    <ShareAltOutlined />
                    分享
                  </span>
                </div>
              </div>
            );
          })}
      </div>
    );
  }
}

export default connect(
  (state) => ({ ...state.ArticleReducer }),
  (dispatch) => bindActionCreators(ArticleAction, dispatch)
)(withRouter(List));
