import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import api from '../../api/index';
export class Tab extends Component {
  state = {
    arr: [],
  };
  async componentDidMount() {
    let res = await api.TabList();
    console.log(res.data.data, 'tab');
    this.setState({
      arr: [{ label: '所有' }, ...res.data.data],
    });
  }
  render() {
    return (
      <ul>
        {/* <li onClick={()=>{
            this.props.history.push({
              pathname: "/home/article",
            });
        }}>所有</li> */}
        {this.state.arr.map((item, index) => {
          return (
            <li
              key={index}
              onClick={() => {
                if (item.label === '所有') {
                  console.log(item, 'fff');
                  this.props.history.push({
                    pathname: '/home/article',
                    state: item.label,
                  });
                } else {
                  this.props.history.push({
                    pathname: '/home/fe',
                    state: item.label,
                  });
                }
              }}
            >
              {item.label}
            </li>
          );
        })}
      </ul>
    );
  }
}

export default withRouter(Tab);
