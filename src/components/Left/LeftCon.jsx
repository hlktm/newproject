import React, { Component } from 'react';
import './LeftCon.css';
import { Badge, Modal, message } from 'antd';
import QRCode from 'qrcode.react';
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
export class LeftCon extends Component {
  state = {
    isModalVisible: false,
    count: 0,
  };
  //显示对话框
  showModal = () => {
    this.setState({
      isModalVisible: true,
    });
  };
  //对话框 下载按钮
  handleOk = () => {
    //错误提示
    message.error('分享海报制作失败,请重新截图');
    this.setState({
      isModalVisible: true,
    });
  };
  //对话框 取消按钮
  handleCancel = () => {
    this.setState({
      isModalVisible: false,
    });
  };
  render() {
    // console.log(this.props.arr, 'arr');
    let { arr } = this.props;
    return (
      <div className="left_con">
        <div>
          {/* 收藏 */}
          <Badge count={this.state.count} className="badges" offset="-9,19">
            <HeartOutlined
              style={{ fontSize: '50px' }}
              onClick={() => {
                this.setState({
                  count: this.state.count + 1,
                });
              }}
            />
          </Badge>
        </div>
        <div>
          {/* 评论 */}
          <EyeOutlined
            onClick={() => {
              //   console.log(2334);
            }}
          />
        </div>
        <div>
          {/* 分享 */}
          <ShareAltOutlined onClick={this.showModal}></ShareAltOutlined>
          <Modal
            title="分享海报"
            visible={this.state.isModalVisible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            className="Modal"
            cancelText="关闭"
            okText="下载"
            width="500px"
            height="300px"
          >
            <img src={arr && arr.cover} alt="" />
            <h1 style={{ fontWeight: 800 }}>{arr && arr.title}</h1>
            <p>{arr && arr.summary}</p>
            <dl>
              <dt>
                <QRCode value={arr && arr.cover} renderAs="canvas" />
              </dt>
              <dd>
                <h4>识别二维码查看文章</h4>
                <p>
                  原文分享自<span style={{ color: '#ff0064' }}>小楼又清风</span>
                </p>
              </dd>
            </dl>
          </Modal>
        </div>
      </div>
    );
  }
}

export default LeftCon;
