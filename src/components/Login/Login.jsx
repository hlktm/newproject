import React, { Component } from 'react';
import './Login.less';
import Icon, { GithubOutlined } from '@ant-design/icons';
import { Menu, Modal, Checkbox, Form, Input, Button } from 'antd';
export class Login extends Component {
  handleOk = () => {
    this.setState({
      [this.props.isModalVisible]: false,
    });
  };

  handleCancel = () => {
    this.setState({
      [this.props.isModalVisible]: false,
    });
  };
  onFinish = (values) => {
    console.log('Success:', values);
  };

  onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  render() {
    return (
      <div className="masterLogin" title="登录遮罩层">
        <div className="login" title="登录页面">
          <Modal
            title="帐密登录"
            footer={null}
            visible={this.props.isModalVisible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={this.onFinish}
              onFinishFailed={this.onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                rules={[
                  {
                    required: true,
                    message: 'Please input your username!',
                  },
                ]}
              >
                <Input placeholder="姓名" />
              </Form.Item>

              <Form.Item
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Input.Password placeholder="密码" />
              </Form.Item>

              <Form.Item
                name="remember"
                valuePropName="checked"
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <GithubOutlined />
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Button type="primary">登录</Button>
              </Form.Item>
            </Form>
          </Modal>
        </div>
      </div>
    );
  }
}

export default Login;
