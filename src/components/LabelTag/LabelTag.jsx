import React, { Component } from 'react';
import axios from 'axios';
import './LabelTag.less';
import api from '../../api/index';
import { withRouter } from 'react-router-dom';
export class LabelTag extends Component {
  state = {
    tagList: [],
  };
  async componentDidMount() {
    // axios.get('/api/tag').then((res) => {
    //   // console.log(res.data);
    //   this.setState({
    //     tagList: res.data.data,
    //   });
    // });
    let res = await api.getTab();
    this.setState({
      tagList: res.data.data,
    });
  }

  render() {
    let { tagList } = this.state;
    console.log(tagList, 'list');
    return (
      <div className="tag" id="labelTag" title="文章标签">
        <h2>文章标签</h2>
        <div className="tabContent">
          {tagList &&
            tagList.map((item, index) => {
              return (
                <span
                  key={index}
                  id={item.id}
                  title={item.value}
                  onClick={() => {
                    console.log(this.props);
                    console.log(item);
                    this.props.history.push({
                      pathname: '/home/' + item.value,
                      state: item,
                    });
                  }}
                >
                  {item.label}[{item.articleCount}]
                </span>
              );
            })}
        </div>
      </div>
    );
  }
}

export default withRouter(LabelTag);
