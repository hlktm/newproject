import React, { Component } from 'react';
import { UserOutlined, SmileOutlined } from '@ant-design/icons';
import '../../style/pinglun.css';

export default class pinglun extends Component {
  render() {
    return (
      <div className="shang">
        <div className="left">
          <UserOutlined />
        </div>
        <div className="right">请输入评论内容(支持Markdown)</div>
        <div className="xia">
          <div className="zuo">
            <SmileOutlined onClick={this.toggle} /> 表情
          </div>
          <button>发布</button>
        </div>
      </div>
    );
  }
}
