import React, { Component } from 'react';
import api from '../../api';
import ReadList from '../ReadList/ReadList';
import './DetailRead.less';
import { withRouter } from 'react-router-dom';
export class DetailRead extends Component {
  state = {
    commend: [],
  };
  async componentDidMount() {
    let res = await api.getArticle();
    this.setState({
      commend: res.data.data[0],
    });
  }
  render() {
    let { commend } = this.state;
    function getDate(item) {
      // console.log(item.publishAt);
      let ztime = Date.now() - new Date(item.publishAt).getTime();
      //秒
      let second = parseInt((ztime / 1000) % 60);
      //分钟
      let minute = parseInt((ztime / 1000 / 60) % 60);
      //小时
      let hour = parseInt((ztime / 1000 / 60 / 60) % 24);
      //天数
      let day = parseInt((ztime / 1000 / 60 / 60 / 24) % 30);
      //月
      let mounth = parseInt((ztime / 1000 / 60 / 60 / 24 / 30) % 12);
      //年
      let year = parseInt(ztime / 1000 / 60 / 60 / 24 / 30 / 12);
      // console.log(`${year}年${mounth}月${day}天${hour}小时${minute}分${second}秒`);
      let ctime =
        year > 0
          ? year + '年'
          : mounth > 0
          ? mounth + '月'
          : day > 0
          ? day + '天'
          : hour > 0
          ? hour + '小时'
          : minute > 0
          ? minute + '分钟'
          : second + '秒';
      return ctime;
    }

    // console.log(commend, 'commr');

    return (
      <div className="recommend" title="推荐阅读">
        <ReadList arr={commend && commend.filter((val) => val.summary)} />
      </div>
    );
  }
}

export default withRouter(DetailRead);
