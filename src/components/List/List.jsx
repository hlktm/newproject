import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as ArticleAction from '../../store/model/Article/articleAction';
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
import Tab from '../Tab/Tab';
import api from '../../api/index';
class List extends Component {
  state = {
    arr: this.props.arr,
  };

  render() {
    function getDate(item) {
      // console.log(item.publishAt);
      let ztime = Date.now() - new Date(item.publishAt).getTime();
      //秒
      let second = parseInt((ztime / 1000) % 60);
      //分钟
      let minute = parseInt((ztime / 1000 / 60) % 60);
      //小时
      let hour = parseInt((ztime / 1000 / 60 / 60) % 24);
      //天数
      let day = parseInt((ztime / 1000 / 60 / 60 / 24) % 30);
      //月
      let mounth = parseInt((ztime / 1000 / 60 / 60 / 24 / 30) % 12);
      //年
      let year = parseInt(ztime / 1000 / 60 / 60 / 24 / 30 / 12);
      // console.log(`${year}年${mounth}月${day}天${hour}小时${minute}分${second}秒`);
      let ctime =
        year > 0
          ? year + '年'
          : mounth > 0
          ? mounth + '月'
          : day > 0
          ? day + '天'
          : hour > 0
          ? hour + '小时'
          : minute > 0
          ? minute + '分钟'
          : second + '秒';
      return ctime;
    }

    let { arr } = this.props;
    return (
      <div className="list">
        <Tab></Tab>
        {(arr && arr.length && this.props.location.state === '所有') ||
        !this.props.location.state
          ? arr &&
            arr.length &&
            arr.map((item, index) => {
              return (
                <div
                  key={index}
                  className="concent"
                  onClick={() => {
                    console.log(item);
                    this.props.history.push({
                      pathname: '/home/detail/' + item.id,
                      state: item,
                    });
                  }}
                >
                  <h1 style={{ fontWeight: 'bolder', fontSize: '20px' }}>
                    {item.title} {this.props.timer ? '' : ''}
                    <span>{item.category && item.category.label}</span>{' '}
                    <span>大约 {getDate(item)} 前</span>
                  </h1>
                  <div className="summary">
                    <p>{item.summary}</p>
                    <img src={item.cover} alt="" />
                  </div>
                  <div className="ico">
                    <span style={{ marginLeft: '20px', fontSize: '20px' }}>
                      {' '}
                      <HeartOutlined />
                      {item.likes}
                    </span>
                    <span style={{ marginLeft: '20px', fontSize: '20px' }}>
                      {' '}
                      <EyeOutlined /> {item.views}
                    </span>
                    <span style={{ marginLeft: '20px', fontSize: '20px' }}>
                      <ShareAltOutlined />
                      分享
                    </span>
                  </div>
                </div>
              );
            })
          : arr &&
            arr.length &&
            arr.filter(
              (val) =>
                val.category && val.category.label === this.props.location.state
            ).length &&
            arr
              .filter(
                (val) =>
                  val.category &&
                  val.category.label === this.props.location.state
              )
              .map((item, index) => {
                // console.log(item,"gggggggggggggg");
                // if(item){
                return (
                  <div
                    key={index}
                    className="concent"
                    onClick={() => {
                      this.props.history.push({
                        pathname: '/home/detail/' + item.id,
                        state: item,
                      });
                    }}
                  >
                    <h1>
                      {item.title}{' '}
                      <span>{item.category && item.category.label}</span>{' '}
                    </h1>
                    <div className="summary">
                      <p>{item.summary}</p>
                      <img src={item.cover} alt="" />
                    </div>
                    <div className="ico">
                      <span>
                        {' '}
                        <HeartOutlined />
                        {item.likes}
                      </span>
                      <span>
                        {' '}
                        <EyeOutlined /> {item.views}
                      </span>
                      <span>
                        <ShareAltOutlined />
                        分享
                      </span>
                    </div>
                  </div>
                );
              })}
        {/* {arr && arr.length
          ?
          : '暂无数据'} */}
      </div>
    );
  }
}

export default connect(
  (state) => ({ ...state.ArticleReducer }),
  (dispatch) => bindActionCreators(ArticleAction, dispatch)
)(withRouter(List));
