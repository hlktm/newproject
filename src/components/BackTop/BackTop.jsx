import React, { Component } from 'react';
import { BackTop } from 'antd';
export class BackTopS extends Component {
  render() {
    return (
      <div>
        {/* 回到顶部 */}
        <BackTop />
      </div>
    );
  }
}

export default BackTopS;
