import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { ChildRoutes } from '../../routes/RouterConfig';
import { Menu, Modal, Checkbox, Form, Input, Button, Space } from 'antd';
import { AudioOutlined } from '@ant-design/icons';
import zhCN from 'antd/lib/locale/zh_CN';
import enCN from 'antd/lib/locale/en_US';
import { ConfigProvider } from 'antd';
import * as Active from '../../store/model/Article/articleAction';
import { connect } from 'react-redux';
import './Header.less';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Login from '../Login/Login';
const { Search } = Input;
export class Head extends Component {
  state = {
    flag: true,
    yuyan: '中文',
    isModalVisible: false,
    isModalSearch: false,
    titleIndex: 0,
    titleArr: [
      { label: '文章', path: '/home/article' },
      { label: '归档', path: '/home/pigeonhole' },
      { label: '知识小册', path: '/home/knowledge' },
      { label: '留言板', path: '/home/message' },
      { label: '关于', path: '/home/about' },
    ],
  };
  changeColor = () => {
    this.setState(
      {
        flag: !this.state.flag,
      },
      () => {
        // 改变主题颜色
        if (this.state.flag) {
          window.less.modifyVars({
            '@bg-color': '#fff',
            '@text-color': '#000',
            '@main-color': '#e7eaee',
          });
        } else {
          window.less.modifyVars({
            '@bg-color': '#001529',
            '@text-color': '#fff',
            '@main-color': '#121418',
          });
        }
      }
    );
  };

  changeLang = (e) => {
    this.props.languageChang(e.target.innerHTML);
  };
  //弹框显示隐藏
  showModal = () => {
    this.setState({
      isModalVisible: true,
    });
  };
  hideModal = () => {
    this.setState({
      isModalSearch: false,
    });
  };
  //搜索
  onSearch = (value) => console.log(value);
  render() {
    // console.log(this.props.language, ' this.props.language');
    let { titleArr, titleIndex } = this.state;
    return (
      <header>
        <div className="head">
          <ul>
            <li>
              <img
                height="36"
                src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png"
                alt="标识"
                title="图片"
              />
            </li>
            {/* <li><span>文章</span></li>
            <li><span>归档</span></li>
            <li><span>知识小册</span></li>
            <li><span>留言板</span></li>
            <li><span>关于</span></li> */}

            {titleArr &&
              titleArr.map((item, index) => {
                return (
                  <li
                    key={index}
                    className={titleIndex === index ? 'activeOn' : ''}
                    onClick={() => {
                      console.log(index);
                      this.props.history.push({
                        pathname: item.path,
                      });
                      this.setState({
                        titleIndex: index,
                      });
                      document.title = item.label;
                    }}
                  >
                    <span>{item.label}</span>
                  </li>
                );
              })}

            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li className="switchover">
              <svg
                viewBox="0 0 1024 1024"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                width="1em"
                height="1em"
                title="语言切换"
              >
                <path
                  d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z"
                  fill="currentColor"
                ></path>
              </svg>
              {/* {this.props.language} 
              <ConfigProvider locale={zhCN}>
               one
              </ConfigProvider> */}
              {this.props.language === '中文' || 'Chinese' ? (
                <ul className="languageUl" onClick={this.changeLang}>
                  <li>英文</li>
                  <li>中文</li>
                </ul>
              ) : (
                <ul className="languageUl" onClick={this.changeLang}>
                  <li>English </li>
                  <li>Cinese</li>
                </ul>
              )}
              {/* {
                this.state.yuyan === "中文" || "Chinese" ?
                  <ul className="languageUl" onClick={this.changeLang}>
                    <li>英文</li>
                    <li>中文</li>
                  </ul> :
                  <ul className="languageUl" onClick={this.changeLang}>
                    <li>English </li>
                    <li>Cinese</li>
                  </ul>
              } */}
            </li>
            <li>
              <div className="motif" onClick={this.changeColor}>
                {this.state.flag ? (
                  <div className="_2BHtRLIf7EUPHvdL0oIKeRS">
                    <svg
                      className="sun"
                      style={{ width: '24px', height: '24px' }}
                      viewBox="0 0 24 24"
                      title="亮色主题"
                    >
                      <path
                        fill="currentColor"
                        d="M3.55,18.54L4.96,19.95L6.76,18.16L5.34,16.74M11,22.45C11.32,22.45 13,22.45 13,22.45V19.5H11M12,5.5A6,6 0 0,0 6,11.5A6,6 0 0,0 12,17.5A6,6 0 0,0 18,11.5C18,8.18 15.31,5.5 12,5.5M20,12.5H23V10.5H20M17.24,18.16L19.04,19.95L20.45,18.54L18.66,16.74M20.45,4.46L19.04,3.05L17.24,4.84L18.66,6.26M13,0.55H11V3.5H13M4,10.5H1V12.5H4M6.76,4.84L4.96,3.05L3.55,4.46L5.34,6.26L6.76,4.84Z"
                      ></path>
                    </svg>
                  </div>
                ) : (
                  <div className="_2BHtRLIf7EUPHvdL0oIKeRM">
                    <svg
                      className="moon"
                      style={{ width: '24px', height: '24px' }}
                      viewBox="0 0 24 24"
                      color="red"
                      title="暗色主题"
                    >
                      <path
                        fill="currentColor"
                        d="M17.75,4.09L15.22,6.03L16.13,9.09L13.5,7.28L10.87,9.09L11.78,6.03L9.25,4.09L12.44,4L13.5,1L14.56,4L17.75,4.09M21.25,11L19.61,12.25L20.2,14.23L18.5,13.06L16.8,14.23L17.39,12.25L15.75,11L17.81,10.95L18.5,9L19.19,10.95L21.25,11M18.97,15.95C19.8,15.87 20.69,17.05 20.16,17.8C19.84,18.25 19.5,18.67 19.08,19.07C15.17,23 8.84,23 4.94,19.07C1.03,15.17 1.03,8.83 4.94,4.93C5.34,4.53 5.76,4.17 6.21,3.85C6.96,3.32 8.14,4.21 8.06,5.04C7.79,7.9 8.75,10.87 10.95,13.06C13.14,15.26 16.1,16.22 18.97,15.95M17.33,17.97C14.5,17.81 11.7,16.64 9.53,14.5C7.36,12.31 6.2,9.5 6.04,6.68C3.23,9.82 3.34,14.64 6.35,17.66C9.37,20.67 14.19,20.78 17.33,17.97Z"
                      ></path>
                    </svg>
                  </div>
                )}
              </div>
            </li>
            <li>
              <svg
                viewBox="64 64 896 896"
                focusable="false"
                data-icon="search"
                width="1em"
                height="1em"
                fill="currentColor"
                aria-hidden="true"
                title="搜索"
                onClick={() => {
                  this.setState({
                    isModalSearch: true,
                  });
                }}
              >
                <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
              </svg>
            </li>
            <li>
              <button
                style={{ outline: 'none', padding: '5px' }}
                onClick={this.showModal}
              >
                登录
              </button>
            </li>
          </ul>
        </div>
        {/* 登录 */}
        <Login isModalVisible={this.state.isModalVisible}></Login>
        {/* 搜索 */}
        <Modal
          title="文章搜索"
          footer={null}
          visible={this.state.isModalSearch}
          onOk={this.handleOk}
          closable
          onCancel={this.handleCancel}
        >
          <Search
            placeholder="输入关键字,搜索文章"
            onSearch={this.onSearch}
            style={{
              width: 304,
            }}
          />
        </Modal>
      </header>
    );
  }
}

// export default withRouter(Head);
export default connect(
  (store) => ({ ...store.ArticleReducer }),
  (dispatch) => bindActionCreators(Active, dispatch)
)(withRouter(Head));
