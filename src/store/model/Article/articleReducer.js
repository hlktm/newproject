const initData = {
  data: [],
  language: '中文',
};
const ArticleReducer = (state = initData, { type, payload }) => {
  let newSate = JSON.parse(JSON.stringify(state));
  switch (type) {
    case 'GET_LIST':
      newSate.data = payload;
      return newSate;
    case 'LANGUAGE_CHANGE':
      newSate.language = payload;
      return newSate;
    default:
      return newSate;
  }
};
export default ArticleReducer;
