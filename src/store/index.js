import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import logger from 'redux-logger';
import ArticleReducer from './model/Article/articleReducer';
const Reducer = combineReducers({
  ArticleReducer,
});
export default createStore(Reducer, applyMiddleware(thunk, logger));
