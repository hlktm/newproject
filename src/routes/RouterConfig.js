import { lazy } from 'react';

export const ChildRoutes = [
  {
    path: '/home/article',
    meta: {
      title: '文章',
    },

    component: lazy(() => import('../Views/Item/article/Article')),
  },
  {
    path: '/home/pigeonhole',
    meta: {
      title: '归档',
    },
    component: lazy(() => import('../Views/Item/Pigeonhole')),
  },
  {
    path: '/home/knowledge',
    meta: {
      title: '知识小册',
    },
    component: lazy(() => import('../Views/Item/Knowledge')),
  },
  {
    path: '/home/message',
    meta: {
      title: '留言板',
    },
    component: lazy(() => import('../Views/Item/Message')),
  },
  {
    path: '/home/detail/:id',
    component: lazy(() => import('../Views/Item/Detail')),
  },
  {
    path: '/home/about',
    meta: {
      title: '关于',
    },
    component: lazy(() => import('../Views/Item/About')),
  },
  //广告图详情
  {
    path: '/home/bannerDetail',
    component: lazy(() => import('../Views/Item/article/bannerDetail')),
  },
  {
    path: '/home/fe',
    component: lazy(() => import('../Views/Item/article/fe')),
  },
  {
    path: '/home/:item',
    component: lazy(() => import('../Views/Item/article/articleTab')),
  },
  {
    path: '/home',
    redirect: '/home/article',
  },
  {
    path: '*',
    component: lazy(() => import('../Views/NoFind')),
  },
];

const Routes = {
  mode: 'history',
  routes: [
    {
      path: '/home',
      component: lazy(() => import('../Views/Home')),
      children: ChildRoutes,
    },

    {
      path: '/',
      redirect: '/home',
    },
    {
      path: '*',
      component: lazy(() => import('../Views/NoFind')),
    },
  ],
};

export default Routes;
