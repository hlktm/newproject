import React, { Suspense } from 'react';

import { Route, Redirect, Switch } from 'react-router-dom';

function RouterView({ routes }) {
  // console.log(routes);
  return (
    <Suspense fallback={<div>loading...</div>}>
      <Switch>
        {routes &&
          routes.map((item, index) => {
            return (
              <Route
                key={index}
                path={item.path}
                render={(propRoute) => {
                  // return item.redirect ? (
                  //   <Redirect exact to={item.redirect} />
                  // ) : (
                  //   <item.component {...propRoute} routes={item.children} />
                  // );
                  if (item.redirect) {
                    return <Redirect to={item.redirect} />;
                  } else if (item.component) {
                    return (
                      <item.component {...propRoute} routes={item.children} />
                    );
                  } else {
                    return <Route path="*" />;
                  }
                }}
              />
            );
          })}
      </Switch>
    </Suspense>
  );
}

export default RouterView;
