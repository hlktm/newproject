import React, { Suspense, lazy } from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';

const Home = lazy(() => import('../Views/Home.jsx'));
const Detail = lazy(() => import('../Views/Detail.jsx'));

const Routes = () => {
  return (
    <BrowserRouter>
      <Suspense fallback={<div>load...</div>}>
        <Route path="/" render={() => <Redirect to="/home" />} />
        <Route path="/home" component={Home} />
        <Route path="/detail" component={Detail} />
      </Suspense>
    </BrowserRouter>
  );
};

export default Routes;
