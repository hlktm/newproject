import React, { Component } from 'react';
import axios from 'axios';
import '../../style/knowledge.moudle.less';
import LabelTag from '../../components/LabelTag/LabelTag';
import Recommend from '../../components/Recommend/Recommend';
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
export class Knowledge extends Component {
  state = {
    list: [],
  };
  componentDidMount() {
    axios.get('/api/knowledge').then((res) => {
      this.setState(
        {
          list: res.data.data,
        },
        () => {
          // console.log(this.state.list, 'state.list');
        }
      );
    });
  }
  render() {
    function getDate(item) {
      // console.log(item.publishAt);
      let ztime = Date.now() - new Date(item.createAt).getTime();
      //秒
      let second = parseInt((ztime / 1000) % 60);
      //分钟
      let minute = parseInt((ztime / 1000 / 60) % 60);
      //小时
      let hour = parseInt((ztime / 1000 / 60 / 60) % 24);
      //天数
      let day = parseInt((ztime / 1000 / 60 / 60 / 24) % 30);
      //月
      let mounth = parseInt((ztime / 1000 / 60 / 60 / 24 / 30) % 12);
      //年
      let year = parseInt(ztime / 1000 / 60 / 60 / 24 / 30 / 12);
      // console.log(`${year}年${mounth}月${day}天${hour}小时${minute}分${second}秒`);
      let ctime =
        year > 0
          ? year + '年'
          : mounth > 0
          ? mounth + '月'
          : day > 0
          ? day + '天'
          : hour > 0
          ? hour + '小时'
          : minute > 0
          ? minute + '分钟'
          : second + '秒';
      return ctime;
    }
    const { list } = this.state;
    return (
      <div className="knowledge">
        <div className="knowledge_left">
          {list[0] &&
            list[0].map((item, index) => {
              return (
                <div
                  className="knowledgelist"
                  key={index}
                  onClick={() => {
                    this.props.history.push({
                      pathname: '/home/detail/' + item.id,
                      state: item,
                    });
                  }}
                >
                  <h1>
                    <b>{item.title}</b>{' '}
                    <span>
                      |{''} {getDate(item)}{' '}
                    </span>
                  </h1>
                  <div className="summary">
                    <p>{item.summary}</p>
                    <img src={item.cover} alt="" />
                  </div>
                  <div className="ico">
                    <span>
                      {' '}
                      <EyeOutlined /> {item.views}
                    </span>
                    <span>
                      <ShareAltOutlined />
                      分享
                    </span>
                  </div>
                </div>
              );
            })}
        </div>
        <div className="knowledge_right">
          <Recommend />
          <LabelTag />
        </div>
      </div>
    );
  }
}

export default Knowledge;
