import React, { Component } from 'react';
import axios from 'axios';
import '../../style/pigeonhole.less';
import Classify from '../../components/Classify/Classify';
import api from '../../api';
import Recommend from '../../components/Recommend/Recommend';
import BackTop from '../../components/BackTop/BackTop';
import LabelTag from '../../components/LabelTag/LabelTag';
export class Pigeonhole extends Component {
  state = {
    list: {},
  };
  async componentDidMount() {
    // axios.get('/api/article/archives').then((res) => {
    //   // console.log(res.data.data);
    //   this.setState({
    //     list: res.data.data,
    //   });
    // });
    let res = await api.getPigeonhole();
    // console.log(res,'res');
    this.setState({
      list: res.data.data,
    });
  }
  render() {
    let { list } = this.state;
    let year = Object.keys(list);
    let data;
    let month;
    if (list[year]) {
      month = Object.keys(list[year]);
      data = list[year][month];
    }
    return (
      <div className="pigeonhole">
        <div className="pigeonhole_head">
          <p>
            <span className="totalConts">归档</span>
          </p>
          <p>
            <span>共计</span>
            <span className="totalConts">{data && data.length}</span>
            <span>篇</span>
          </p>
        </div>
        <div className="pigeonhole_contents">
          <h1>{year && year}</h1>
          <h3>{month && month}</h3>
          <ul className="pigeonhole_cont" ref={(el) => (this.contentEl = el)}>
            {/* 列表渲染 */}
            {data &&
              data.map((item, index) => {
                return (
                  <li
                    key={index}
                    //跳详情页
                    onClick={() => {
                      this.props.history.push({
                        pathname: '/home/bannerDetail',
                        state: item,
                      });
                    }}
                  >
                    <span>
                      <time>4-12</time>
                    </span>
                    <span>{item.title}</span>
                  </li>
                );
              })}
          </ul>
        </div>
        <div className="right">
          <Recommend />
          <Classify></Classify>
        </div>
        <BackTop></BackTop>
      </div>
    );
  }
}

export default Pigeonhole;
