import React, { Component } from 'react';
import '../../style/About.css';

export class About extends Component {
  componentDidMount() {
    // window.document.title = '关于';
  }
  render() {
    return (
      <div className="about">
        <img
          src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg"
          alt=""
        />
        <h1>
          这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。
        </h1>
        <p>更新</p>
        <ul>
          <li>新增Github Oauth登录，由于网络原因，容易失败，可以多尝试几下</li>
          <li>1</li>
          <li>2</li>
        </ul>
      </div>
    );
  }
}

export default About;
