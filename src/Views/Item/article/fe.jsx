import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as ArticleAction from '../../../store/model/Article/articleAction';
import List from '../../../components/List/List';
import './style/fe.less';
import Recommend from '../../../components/Recommend/Recommend';
import LabelTag from '../../../components/LabelTag/LabelTag';
import Icon from '../../../components/Icon/Icon';
import api from '../../../api/index';
export class fe extends Component {
  state = {
    arr: [],
    newArr: [],
  };
  async componentDidMount() {
    let res = await api.getList();
    this.setState({
      arr: res.data.data[0],
    });
  }

  render() {
    // this.setState({
    //   arr: this.state.arr.filter((item, index) => {
    //     // console.log(item,"item");
    //     return item.category != null && item.category.label.includes(this.props.location.state)
    //   })
    // })
    // console.log(this.state.arr);
    // console.log(this.props);
    // console.log(this.props.location.state, "00030");
    console.log(this.state.arr);
    return (
      <div className="fe">
        <div className="top">
          <p className="title">
            <span>{this.props.location.state}</span> 分类文章
          </p>
          <p>
            共搜索到
            {this.state.arr &&
              this.state.arr.filter(
                (item) => item.category.label === this.props.location.state
              ).length}
            篇
          </p>
        </div>
        <List arr={this.state.arr && this.state.arr}></List>

        <div className="right">
          <Recommend />
          <LabelTag />
          <Icon />
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({ ...state.ArticleReducer }),
  (dispatch) => bindActionCreators(ArticleAction, dispatch)
)(fe);
