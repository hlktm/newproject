import React, { Component } from 'react';
import './style/articleTab.less';
import Recommend from '../../../components/Recommend/Recommend';
import LabelTag from '../../../components/LabelTag/LabelTag';
import Icon from '@ant-design/icons/lib/components/Icon';
import api from '../../../api';
import {
  HeartOutlined,
  EyeOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
export class articleTab extends Component {
  state = {
    articleTabList: [],
    tabConList: [],
  };
  async componentDidMount() {
    // 文章标签
    let res = await api.getTab();
    this.setState({
      articleTabList: res.data.data,
    });
    // 文章内容
    let tabcon = await api.getArticle();
    this.setState({
      tabConList: tabcon.data.data[0],
    });
  }
  render() {
    let { state } = this.props.location;
    let { articleTabList, tabConList } = this.state;
    // console.log(tabConList, "tabConList");
    return (
      <div className="articleTab">
        <div className="articleTabTop">
          <p className="articleTabTitle">
            <span> 与 {state.value} 标签有关的文章</span>
          </p>
          <p>共搜索到{state.articleCount}篇</p>
        </div>
        <div className="articleTabTab">
          <p>文章标签</p>
          <p>
            {articleTabList &&
              articleTabList.map((item, index) => {
                return (
                  <span
                    key={index}
                    id={item.id}
                    title={item.value}
                    onClick={() => {
                      // console.log(this.props);
                      // console.log(item, 'hhhhhhhhhhh');
                      this.props.history.push({
                        pathname: '/home/' + item.value,
                        state: item,
                      });
                    }}
                  >
                    {item.label}[{item.articleCount}]
                  </span>
                );
              })}
          </p>
        </div>
        <div className="articleTabCon">
          {/* <List arr={articleTabList && articleTabList.filter(item => item.value === state.value)}></List> */}
          {tabConList &&
          tabConList.filter(
            (item) => item.category.label && item.category.label === state.value
          ).length
            ? tabConList
                .filter((item) => item.category.label === state.value)
                .map((item, index) => {
                  return (
                    <div
                      key={index}
                      className="concent"
                      onClick={() => {
                        this.props.history.push({
                          pathname: '/home/detail/' + item.id,
                          state: item,
                        });
                      }}
                    >
                      <p>
                        <b> {item.title}</b>
                        <span>{item.category && item.category.label}</span>{' '}
                      </p>
                      <div className="summary">
                        <p>{item.summary}</p>
                        <img src={item.cover} alt="" />
                      </div>
                      <div className="ico">
                        <span>
                          {' '}
                          <HeartOutlined />
                          {item.likes}
                        </span>
                        <span>
                          {' '}
                          <EyeOutlined /> {item.views}
                        </span>
                        <span>
                          <ShareAltOutlined />
                          分享
                        </span>
                      </div>
                    </div>
                  );
                })
            : '暂无数据'}
        </div>

        <div className="right">
          <Recommend />
          <LabelTag />
          <Icon />
        </div>
      </div>
    );
  }
}

export default articleTab;
