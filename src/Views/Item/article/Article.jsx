import React, { Component } from 'react';
import LabelTag from '../../../components/LabelTag/LabelTag';
import Recommend from '../../../components/Recommend/Recommend';
import Icon from '../../../components/Icon/Icon';
import List from '../../../components/List/List';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as ArticleAction from '../../../store/model/Article/articleAction';
import './style/article.less';
import api from '../../../api/index';
export class Article extends Component {
  state = {
    arr: [],
  };
  async componentDidMount() {
    let res = await api.getList();
    console.log(res, 'reff');
    this.setState({
      arr: res.data.data[0],
    });
  }
  render() {
    let { arr } = this.state;
    return (
      <div className="article">
        <div className="left">
          {/* 广告图部分 */}
          <div
            className="banner"
            onClick={() => {
              this.props.history.push({
                pathname: '/home/bannerDetail',
                state: arr[2],
              });
            }}
          >
            <img src={arr[2] && arr[2].cover} alt="" />
            <div className="imgMack">{arr[2] && arr[2].title}</div>
          </div>
          {/* 数据列表部分 */}
          <List arr={arr.length && arr}></List>
        </div>
        <div className="right">
          <Recommend />
          <LabelTag />
          <Icon />
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({ ...state.ArticleReducer }),
  (dispatch) => bindActionCreators(ArticleAction, dispatch)
)(Article);
