import React, { Component } from 'react';
import LeftCon from '../../../components/Left/LeftCon';
import './style/bannerDetail.less';
export class bannerDetail extends Component {
  state = {
    arr: {},
  };
  componentDidMount() {
    this.setState({
      arr: this.props.location.state,
    });
  }
  render() {
    let { arr } = this.state;

    return (
      <div className="bannerDetail">
        <div className="left">
          <div className="banner">
            <img src={arr.cover} alt="" />
            <h1>{arr.title}</h1>
            <div dangerouslySetInnerHTML={{ __html: arr.html }}></div>
          </div>
          {/* 推荐阅读 */}
          <h2>推荐阅读</h2>
          <LeftCon arr={arr} />
        </div>
        {/* 楼层 */}
        <div className="right">
          <h2>{arr.title}</h2>
          <ul>
            <li>什么是 Markdown</li>
            <li>1. 待办事宜 Todo 列表</li>
            <li>2. 高亮一段代码[^code]</li>
            <li>3. 绘制表格</li>
            <li>4嵌入网址</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default bannerDetail;
