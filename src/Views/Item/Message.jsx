import React, { Component } from 'react';
import '../../style/Message.css';
// import Messagelby from '../../components/Messagelyb/Messagelby';
import axios from 'axios';
import PingLun from '../../components/pinglun/pinglun';
import { Pagination } from 'antd';
export class Message extends Component {
  state = {
    arr: [],
    pageSize: 1,
    pageNum: 2,
    newArr: [],
  };
  componentDidMount() {
    let { pageSize, pageNum } = this.state;
    axios.get('/api/comment').then((res) => {
      console.log(res.data.data[0], 'list');
      this.setState({
        arr: res.data.data[0].slice(
          (pageSize - 1) * pageNum,
          pageSize * pageNum
        ),
        newArr: res.data.data[0],
      });
      console.log(this.state.arr);
    });
  }

  render() {
    let { arr, newArr } = this.state;

    return (
      <div className="message">
        <div className="tou">
          <h2>留言板</h2>
          <p>[请勿灌水]</p>
          <p>[垃圾评论过多，欢迎提供好的过滤(标记)算法]</p>
        </div>

        <div className="pp">
          {' '}
          <h1>评论</h1>
          <PingLun />
        </div>

        <div className="shen">
          {arr &&
            arr.map((item, index) => {
              return (
                <div key={index} className="content1">
                  <div className="left1">
                    <div className="toux"></div>{' '}
                    <h3 key={index}>{item.name}</h3>
                  </div>
                  <div className="right1">
                    <p>{item.content}</p>
                    <p>{item.createAt}</p>
                  </div>
                </div>
              );
            })}
          {/* <div className='Fye'><Pagination total={newArr.length} pageSize={3} current={this.state.pageSize} onChange={this.onChange} /></div> */}
          <Pagination defaultCurrent={1} total={50} />
          {/* <Messagelby /> */}
        </div>
      </div>
    );
  }
}

export default Message;
