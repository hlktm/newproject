import React, { Component } from 'react';
import './article/style/bannerDetail.less';
import DetailRead from '../../components/DetailRead/DetailRead';
export class Detail extends Component {
  render() {
    console.log(this.props.location.state);
    let { state } = this.props.location;
    return (
      <div className="bannerDetail">
        <div className="left">
          <div className="banner">
            <img src={state.cover} alt="" />
            <h1>{state.title}</h1>
            <div dangerouslySetInnerHTML={{ __html: state.html }}></div>
          </div>
          {/* 推荐阅读 */}
          <div className="Reading">
            <h1
              style={{
                fontSize: '20px',
                fontWeight: 'bold',
                textAlign: 'center',
              }}
            >
              推荐阅读
            </h1>
            <DetailRead />
          </div>
        </div>
        {/* 楼层 */}
        <div className="right">
          <h2>{state.title}</h2>
          <ul>
            <li>什么是 Markdown</li>
            <li>1. 待办事宜 Todo 列表</li>
            <li>2. 高亮一段代码[^code]</li>
            <li>3. 绘制表格</li>
            <li>4嵌入网址</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Detail;
