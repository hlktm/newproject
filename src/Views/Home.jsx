import React, { Component, lazy } from 'react';
import Head from '../components/Head/Head';
import Login from '../components/Login/Login';
import RouterView from '../routes/RouterViews';
export class Home extends Component {
  render() {
    // console.log(this);
    return (
      <div className="home">
        <Head />
        <Login />
        <main>
          <div className="main" title="页面">
            <div className="content" title="内容">
              <RouterView routes={this.props.routes} />
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default Home;
