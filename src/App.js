import './App.css';
import RouterViews from './routes/RouterViews';
import RouteConfig from './routes/RouterConfig';
import { BrowserRouter, HashRouter } from 'react-router-dom';
function App() {
  // console.log(this);
  let { mode = 'history', routes } = RouteConfig;
  // console.log(mode, routes);

  return mode === 'history' ? (
    <BrowserRouter>
      <RouterViews routes={routes} />;
    </BrowserRouter>
  ) : (
    <HashRouter>
      <RouterViews routes={routes} />;
    </HashRouter>
  );
}

export default App;
