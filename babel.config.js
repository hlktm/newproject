// 这是项目发布阶段需要用到的babel插件
const plugins = [];
if (process.env.NODE_ENV === 'production') {
  //只有开发环境的时候，才来移除console
  plugins.push('transform-remove-console');
}

module.exports = {
  presets: ['react-app'],
  plugins,
};
