module.exports = {
  extends: ['react-app', 'react-app/jest', 'plugin:prettier/recommended'],
  rules: {
    'prettier/prettier': 'warn',
    //在生产环境下  不能出现打印，弹框，打断点
    'no-console': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-alert': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-debugger': process.env.NODE_ENV === 'development' ? 0 : 1,
    'no-unused-vars': ['warn', { vars: 'all', args: 'none' }],
  },
};
